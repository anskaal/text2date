
var assert = require('assert');
var client = require('./../index');

describe('Client', function() {

  it('Should be able to get date from hours and minutes', function(done){

    var responses = 0;
    client.on("date", function(text, date){
      if(date){
        responses++;
      }
    });

    var tests = [
      "Hey 8:00!",
      "What about 16:01",
      "What about 1:01",
      "No, 17 you dummy. 17:30"
    ];

    for(var t in tests){
        var test = tests[t];
        client.analyze(test);
    }

    if(responses === tests.length){
      done();
    }
  });

  it('Should handle strings without timestamps', function(done){

    var responses = 0;
    client.on("date", function(text, date){
      if(date){
        responses++;
      }
    });

    var tests = [
      "Hey",
      "What about",
      "What about",
      "No, 17 you dummy."
    ];

    for(var t in tests){
        var test = tests[t];
        client.analyze(test);
    }

    if(responses === tests.length){
      done();
    }

    done();
  });

});
