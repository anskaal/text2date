
module.exports = function(){

  var EventEmitter = require('events');
  var util = require('util');

  function MyEmitter() {
    EventEmitter.call(this);
  }

  MyEmitter.prototype.analyze = function(text, callback){

    var time = text.match(/([0-9]{1,2}):([0-9]{2})/i);

    if(!time){
        return;
    }

    if(time.length < 1){
        return;
    }

    var now = new Date();
    now.setHours(time[1]);
    now.setMinutes(time[2]);

    this.emit('date', text, now);

    if(callback !== undefined){
      return callback(now);
    }
  };

  util.inherits(MyEmitter, EventEmitter);

  return new MyEmitter();
}();
